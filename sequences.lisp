(require :sb-mpfr)
(sb-mpfr:set-precision 256) ; 256 bits

; Helper functions
; ================

; piece of code used to add exactly 'n' values in front of 'l'
; if at least one value is added, then the first one is 'v'
(defun myfill (n v l)
  (if (> n 0)
      (loop
        for i from 1 to n
        for k = l then (cons 0 k)
        finally (return (cons v k)))
      l))
;   (defmacro myfill (n v l)
;     `(if (> ,n 0)
;          (loop
;             for i from 1 to ,n
;             for k = ,l then (cons 0 k)
;             finally (return (cons ,v k)))
;           ,l))

; create shared instances of a binomial function with precomputed factorials
; usage:   (defvar B (funcall *binomial-maker* 15))
; (argument is the highest required factorial)
; return the array of factorial as a second value
(defvar *binomial-maker*
  (let ((factorial
          (make-array '(1) :initial-element 1
                           :adjustable T)))
    (lambda (nmax)
      (values
        (let ((s (car (array-dimensions factorial))))
          (loop
            for i from s to nmax
            do (setf (aref factorial i) (* i (aref factorial (1- i))))
            initially
              (if (>= nmax s)
                (setq factorial (adjust-array factorial (list (1+ nmax)))))
            finally (return
          	      (lambda (n k)
                          (/ (aref factorial n)
                             (* (aref factorial k) (aref factorial (- n k)))))))) factorial))))


; Convolution between two series (lists of coefficients); the final size is the
; size of the shortest list
(defun convolution (a b)
  (loop
    for NIL in a
    for y in b
    for z = (list (car b)) then (cons y z)
    collect (loop
              for i in a
              for j in z
              sum (* i j))))


; Compute the reciprocal of a series (list of coefficient); the first coefficient
; MUST not be zero.
(defun convolution-reciprocal (l)
  (loop
    for NIL in l
    for m = (list (/ 1 (car l))) then
      (cons (/ (-
        (loop
          for i in (cdr l)
          for j in m
          sum (* i j)))
        (car l)) m)
    finally (return (nreverse m))))



; Main functions
; ==============

; (setq next (patterns-generator))
; (funcall next)  ==>  (1)
; (funcall next)  ==>  (0 1)
; etc.
(defun patterns-generator (&key negative)
  "Create a generator of simple patterns; each new call to the returned function will yield the next pattern."
  (let ((pattern NIL))
    (lambda ()
      (setq pattern
        (loop
          for size from 0
          for sum = 0 then (+ sum (abs (car p)))
          for p on pattern
          when (and negative (> (car p) 0))
              return (myfill size sum (cons (- (car p)) (cdr p)))
          when (> sum 0)
              return (myfill size (1- sum) (cons (1+ (abs (car p))) (cdr p)))
          finally (return 
                    (cond
                      ; start exploring patterns of a new size
                      ((= size sum) (myfill size 0 '(1)))
                      ; start exploring patterns of size = sum
                      ((= size (1+ sum)) (myfill size size NIL))
                      ; continue exploring patterns with sum < size
                      (T (myfill (1- size) 1 (list sum))))))))))


; Map function for a generator; return a new generator embedding the initial
; one; an optional filter is allowed.
(defun map-generator (f generator &key filter)
  (lambda ()
    (loop
      for s = (funcall f (funcall generator))
              then (funcall f (funcall generator))
      do (if filter
             (if (funcall filter s) (return s))
             (return s)))))

; Iterates over identity transforms of sequences (for ad-hoc purposes)
(defun identity-transform-sequences (size generator &key filter)
  (map-generator (lambda (pat) pat) generator :filter filter))


; Iterates over Euler transforms of sequences; the generator adds an implicit 1
; at the beginning of all pattern; thus the pattern is read as containing
; coefficients starting from index 1 (in order to avoid leading 0).
(defun euler-transform-sequences (size generator &key filter)
  (let* (
    ; first variable in the initial 'let'
    (pascal-row
      (let ((rows (make-array '(1) :adjustable T :initial-contents '((1)))))
        (lambda (n)
          (let ((s (car (array-dimensions rows))))
            (loop
              for i from s to n
              do (setf (aref rows i)
                   (let ((prev (aref rows (1- i))))
                     (loop
                       for acc = '(1) then (cons (+ a b) acc)
                       for a in prev
                       for b in (cdr prev)
                       finally (return (cons 1 acc)))))
              initially (if (>= n s) (setq rows (adjust-array rows (list (1+ n)))))
              finally (return (aref rows n)))))))
    ; second variable in the initial 'let'
    (binomial-power
      (lambda (i n)
        (loop
          with vec = (make-array (list size) :initial-element 0)
          for j from 0 to (1- size) by i
          for k in (funcall pascal-row n)
          for s = 1 then (- s)
          do (setf (aref vec j) (* s k))
          finally (return (coerce vec 'list)))))
    ; third variable in the initial 'let'
    (euler-term
      (let ((cache (make-array '(0) :adjustable T)))
        (lambda (i n)
          (if (< n 0) (funcall binomial-power i (- n))
            (let ((s (car (array-dimensions cache))))
              (loop
                for j from s to i
                do (setf (aref cache j) (make-array '(0) :adjustable T))
                initially (if (>= i s)
                            (setq cache (adjust-array cache (list (1+ i)))))
                finally (return
                  (let ((cache2 (aref cache i)))
                    (progn
                      (if (>= n (car (array-dimensions cache2)))
                          (setq cache2 (adjust-array cache2 (list (1+ n))
                                                     :initial-element NIL)))
                      (if (aref cache2 n) (aref cache2 n)
                            (setf (aref cache2 n)
                              (convolution-reciprocal
                                (funcall binomial-power i n)))))))))))))
    ; fourth variable in the initial 'let'
    (euler-transform
      (lambda (pat)
        (let ((seq
                (loop
                  for i from 1
                  for n in pat
                  when (/= n 0) collect (funcall euler-term i n))))
          (loop
            for result = (car seq) then (convolution result s)
            for s in (cdr seq)
            finally (return result)))))
    )
    ; end of all variables in the initial 'let'
    (map-generator euler-transform generator :filter filter)))


; This iterator yields binomial transforms of patterns (generated by
; another generator), with the OEIS "definition" of the Binomial transform
; (which is not the most "official" one).
(defun binomial-transform-sequences (size generator &key filter)
  (let* (
    ; first variable in the initial 'let'
    (binomial (funcall *binomial-maker* (1- size)))
    ; second variable in the initial 'let'
    (binomial-transform
      (lambda (pat)
        (loop
          for n from 0 to (1- size)
          collect (loop
                    for a in pat
                    for k from 0 to n
                    sum (* (funcall binomial n k) a)))))
    )
    ; end of all variables in the initial 'let'
    (map-generator binomial-transform generator :filter filter)))

; Other definition of the binomial transform (from the Wikipedia page)
; ; the 'variant' keyword uses a different non-invertible definition
(defun binomial-transform-sequences* (size generator &key variant filter)
  (let* (
    ; first variable in the initial 'let'
    (binomial (funcall *binomial-maker* (1- size)))
    ; second variable in the initial 'let'
    (binomial-transform*
      (lambda (pat)
        (loop
          for n from 0 to (1- size)
          for s0 = 1 then (if variant (- s0) 1)
          collect (loop
                    for a in pat
                    for k from 0 to n
                    for s = s0 then (- s)
                    sum (* s (funcall binomial n k) a)))))
    )
    ; end of all variables in the initial 'let'
    (map-generator binomial-transform* generator :filter filter)))

(defun stirling-transform-sequences (size generator &key filter)
  (let* (
    ; first variable in the initial 'let'
    (stirling-number2
      (multiple-value-bind (binomial factorial)
        (funcall *binomial-maker* (1- size))
        (lambda (n k)
          (/ (loop
               for s = 1 then (- s)
               for i from 0 to k
               sum (* s (funcall binomial k i) (expt (- k i) n)))
             (aref factorial k)))))
    ; second variable in the initial 'let'
    (stirling-transform
      (lambda (pat)
        (loop
          for n from 0 to (1- size)
          collect (loop
                    for a in pat
                    for k from 0 to n
                    sum (* (funcall stirling-number2 n k) a)))))
    )
    ; end of all variables in the initial 'let'
    (map-generator stirling-transform generator :filter filter)))

; add 0 in front of the sequence and then apply the exp transform
(defun exp-transform-sequences (size generator &key filter)
  (let* (
    ; first variable in the initial 'let'
    (fact (multiple-value-bind (binomial factorial) (funcall *binomial-maker* (1- size)) factorial))
    ; second variable in the initial 'let'
    (exp-transform
      (lambda (pat)
        (loop
          with b = (cons 0
                     (loop
                       for i from 1 to (1- size)
                       for l = pat then (if (cdr l) (cdr l) '(0))
                       collect (/ (car l) (aref fact i))))
          for i from 0 to (1- size)
          for c = (cons 1 (make-list (1- size) :initial-element 0))
                  then (convolution b c)
          for d = c then (loop
                           for j in d
                           for k in c
                           collect (+ j (/ k (aref fact i))))
          finally (return (loop
                            for j from 0
                            for k in d
                            collect (* k (aref fact j)))))))
    )
    ; end of all variables in the initial 'let'
    (map-generator exp-transform generator :filter filter)))



         


; Meta-generators
; ===============

; Functions creating a new generator from another generator



; Sequentially generate couples of two sequences
; A/B and B/A are both generated but A/A is generated once.
(defun cross-self (generator)
  (let ((l NIL) (s NIL) (r T))
    (lambda ()
      (if r
          (if s
            (progn
              (setq r NIL)
              (list (car l) (car s)))
            (progn
              (setq s l)
              (setq l (cons (funcall generator) l))
              (list (car l) (car l))))
          (let ((v (car s)))
            (progn
              (setq r T)
              (setq s (cdr s))
              (list v (car l))))))))

; Variant of the previous generator; generate couples of two sequences
; with at least one sequence macthing a filter
(defun cross-self* (generator filter)
  (let ((l NIL) (s NIL) (r T))
    (lambda ()
      (if r
          (if s
            (progn
              (setq r NIL)
              (list (car l) (car s)))
            (loop
              for v = (funcall generator) then (funcall generator)
              until (funcall filter v)
              do (setq l (cons v l))
              finally (progn
                        (setq s l)
                        (setq l (cons v l))
                        (return (list v v)))))
          (let ((v (car s)))
            (progn
              (setq r T)
              (setq s (cdr s))
              (list v (car l))))))))

; cross-product between two different generators
; (order between two sequences is preserved)
(defun cross (a b)
  (let ((state NIL) (a1 (list (funcall a))) (b1 NIL) (stack NIL))
    (lambda ()
      (if state
	; first case, use (car b1) with all items in a1
	(if stack
	  (prog1
	    (list (car stack) (car b1))
	    (setq stack (cdr stack)))
	  (progn
	    (setq state NIL)
	    (setq stack (cdr b1))
	    (setq a1 (cons (funcall a) a1))
	    (list (car a1) (car b1))))
	; second case, use (car a1) with all items in b1
	(if stack
	  (prog1
	    (list (car a1) (car stack))
	    (setq stack (cdr stack)))
	  (progn
	    (setq state T)
	    (setq stack (cdr a1))
	    (setq b1 (cons (funcall b) b1))
	    (list (car a1) (car b1))))))))

(defun cross* (a b filter)
  (let ((c (cross a b)))
    (lambda ()
      (loop
        for x = (funcall c) then (funcall c)
        when (funcall filter x) return x))))

(defun filter (f filt)
  (lambda ()
    (loop
      for x = (funcall f) then (funcall f)
      when (funcall filt x) return x)))

; iterate over quotient of terms between sequences from two generators
(defun quotient-generator (a b)
  (let ((c (cross a b)))
    (lambda ()
      (let ((s (funcall c)))
        (loop
          for i in (car s)
          for j in (cadr s)
          collect (/ i j))))))


; make the generator echo n times the same value
; (useful for using the same generator as a seed
; for several other functions)
(defun echo-generator (generator n)
  (let ((s 0) (v NIL))
    (lambda ()
      (if (zerop s)
	(progn
    	  (setq v (funcall generator))
	  (setq s (1- n))
	  v)
	(progn
	  (setq s (1- s))
	  v)))))

; create a circular list  (create-circular-list 1 2 3)
(defun create-circular-list (first &rest rest)
  (let ((items (cons first rest)))
    (setf (cdr (last items)) items)))

; mix togeter several generators all seeded with the same generator
; each meta-generator has to be created with the syntax:
;    (GEN size generator)
; Usage:   (mix-generator (patterns-generator) 128 (list #'euler-transform-sequences
;                                                        #'binomial-transform-sequences))
(defun mix-generator (generator size l &key filter)
  (let*
    ((g2 (echo-generator generator (length l)))
     (l2 (apply #'create-circular-list
		(mapcar (lambda (e) (funcall e size g2)) l)))
     (f (lambda ()
	  (prog1
	    (funcall (car l2))
            (setq l2 (cdr l2))))))
     (lambda ()
       (loop
         for s = (funcall f) then (funcall f)
         do (if filter
                (if (funcall filter s) (return s))
                (return s))))))
      

; simple circular generator
(defun circular-generator (l)
  (let ((g (apply #'create-circular-list l)))
    (lambda ()
      (prog1
        (funcall (car g))
        (setq g (cdr g))))))


; Common functions related on sequences
; =====================================



; differentiate sequence
(defun diff (s)
  (loop
    for v on s
    while (cdr v)
    collect (- (cadr v) (car v))))

; differentiate n times
(defun diffn (s n)
  (loop
    for i from 1 to n
    for v = (diff s) then (diff v)
    finally (return v)))

; test increasing sequence (default  non strict allows repeating terms)
(defun increasing (s &key strict)
  (loop
    for v on s
    while (cdr v)
    when (and strict (>= (car v) (cadr v))) return NIL
    when (> (car v) (cadr v)) return NIL
    finally (return T)))

; increasing non polynomial function
(defun increasing-non-polynomial (s)
  (if (increasing s) (not (member 0 (diffn s 16))) NIL))

; increasing non polynomial function (strictly increasing)
(defun increasing-non-polynomial* (s)
  (if (increasing s :strict T) (not (member 0 (diffn s 16))) NIL))


; generate rational numbers
(defun rational-generator (&key zero negative)
  (let ((a 1) (b (if (and zero (not negative)) 0 1)) (p (and zero negative) ))
    (lambda ()
      (loop
        do (progn
             (if (and p negative)
               (progn
                 (setf p NIL)
                 (return (- (/ (- a b) b)))))
             (setf b (1+ b))
             (if (> b a)
               (progn
                 (setf a (1+ a))
                 (setf b 1)))
             (let ((x (/ (- a b) b)))
               (if (= b (denominator x))
                 (progn
                   (setf p T)
                   (return x)))))))))


; generate tuples of n rationals (n >= 1)
(defun rational-tuple-generator (n &key zero negative)
  (let ((l (loop
             for i from 1 to n
             for x = (rational-generator :zero zero :negative negative)
                   then (cross x (rational-generator :zero zero :negative negative))
             finally (return x))))
    (if (= n 1)
      (lambda () (list (funcall l)))
      (lambda ()
        (loop
          for i from 2 to n
          for a = (funcall l) then (cons (caar a) (cons (cadar a) (cdr a)))
          finally (return a))))))

; Hypergeometric stuff
; ====================

; generate hypergeopetric functions of type aFb as two tuples of tuples
; for instance 2F1 (1, n+1/3; n+2/3; z) will be represented as:
; (((1 1/3) (2/3)) ((0 1) (1)) z)
; a finite set of values for z has to be provided;
(defun hypergeometric-generator (a b z)
  (let ((g (cross-self
             (cross
               (rational-tuple-generator a :zero T :negative T)
               (rational-tuple-generator b :zero T :negative T))))
        (z* NIL) (x NIL))
    (lambda ()
      (if z*
        (prog1
          (list (car x) (cadr x) (car z*))
          (setf z* (cdr z*)))
        (progn
          (setf x (funcall g))
          (setf z* (cdr z))
          (list (car x) (cadr x) (car z)))))))

; quick evaluation (no division by 0 test)
(defun quick-hypergeometric-evalf (f n)
  (loop
    with g = 1
    with s = 1
    for i from 1 to 32
    do (progn
         (loop
           for k1 in (caar f)
           for k2 in (caadr f)
           do (setf g (* g (+ k1 (* k2 n) (1- i))))) 
         (loop
           for k1 in (cadar f)
           for k2 in (cadadr f)
           do (setf g (/ g (+ k1 (* k2 n) (1- i))))) 
         (setf g (/ (* g (caddr f)) i))
         (setf s (+ s (coerce g 'double-float))))
    finally (return s)))

(defun lexicographically-ordered-hypergeometric (f)
  (and
    (loop ; numerators
      for k on (mapcar #'list (caar f) (caadr f))
      while (cdr k)
      always (or
               (< (caar k) (caadr k))
               (and (= (caar k) (caadr k)) (<= (cadar k) (cadadr k)))))
    (loop ; denominators
      for k on (mapcar #'list (cadar f) (cadadr f))
      while (cdr k)
      always (or
               (< (caar k) (caadr k))
               (and (= (caar k) (caadr k)) (<= (cadar k) (cadadr k)))))))



; test for hypergeometric function:
;   * not all coefficients for n are allowed to be 0;
;   * convergence is checked
(defun interesting-hypergeometric-test (f)
  (and
    ; first condition: not all 0 among the coefficients of n
    (or (notevery #'zerop (caadr f)) (notevery #'zerop (cadadr f)))
    ; second condition : lexicography ordered terms (for avoidind some duplicates)
    (lexicographically-ordered-hypergeometric f)
    ; third condition: no same element in numerator and denominator
    (not (intersection
           (mapcar #'list (caar f) (caadr f))
           (mapcar #'list (cadar f) (cadadr f))
           :test #'equal))
    ; fourth condition: convergence
    (loop
      for n from 1 to 128 ; test for some values of n
      always (loop named test
                with g = 1L0
                for i from 1 to 256 ; test initial terms of the series (accuracy)
                always (progn
                         (loop
                           for k1 in (caar f)
                           for k2 in (caadr f)
                           do (setf g (* g (+ k1 (* k2 n) (1- i))))) 
                         (loop
                           for k1 in (cadar f)
                           for k2 in (cadadr f)
                           do (let ((d (+ k1 (* k2 n) (1- i))))
                                (if (zerop d)
                                  (return-from test NIL)
                                  (setf g (/ g d)))))
                         (setf g (/ (* g (caddr f)) i))
                         (or (< i 128) (< (abs g) 1e-12)))))))

; test for hypergeometric function:
;   * not all coefficients for n are allowed to be 0;
;   * convergence is checked
;   * values are all between 0 and 1
(defun interesting-hypergeometric-test2 (f)
  (and
    ; first condition: not all 0 among the coefficients of n
    (or (notevery #'zerop (caadr f)) (notevery #'zerop (cadadr f)))
    ; second condition : lexicography ordered terms (for avoidind some duplicates)
    (lexicographically-ordered-hypergeometric f)
    ; third condition: no same element in numerator and denominator
    (not (intersection
           (mapcar #'list (caar f) (caadr f))
           (mapcar #'list (cadar f) (cadadr f))
           :test #'equal))
    ; fourth condition: convergence
    (loop
      for n from 1 to 128 ; test for some values of n
      always (loop named test
                with g = 1L0
                for i from 1 to 256 ; test initial terms of the series (accuracy)
                always (progn
                         (loop
                           for k1 in (caar f)
                           for k2 in (caadr f)
                           do (setf g (* g (+ k1 (* k2 n) (1- i))))) 
                         (loop
                           for k1 in (cadar f)
                           for k2 in (cadadr f)
                           do (let ((d (+ k1 (* k2 n) (1- i))))
                                (if (zerop d)
                                  (return-from test NIL)
                                  (setf g (/ g d)))))
                         (setf g (/ (* g (caddr f)) i))
                         (or (< i 128) (< (abs g) 1e-12)))))
    (every (lambda (e) (and (sb-mpfr:>= e (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float))
                            (sb-mpfr:<= e (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float))))
           (hypergeometric-eval-vector f))))


(defun interesting-hypergeometric-generator (a b z)
  (filter (hypergeometric-generator a b z) #'interesting-hypergeometric-test))
(defun interesting-hypergeometric-generator2 (a b z)
  (filter (hypergeometric-generator a b z) #'interesting-hypergeometric-test2))


; Misc
; ====

(defun takagi (x)
  (loop
    with s = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for z = (sb-mpfr:coerce x 'sb-mpfr:mpfr-float)
          then (sb-mpfr:mul-2-raised z 1)
    for n = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
          then (sb-mpfr:mul-2-raised n 1)
    when (sb-mpfr:zerop (sb-mpfr:fractional z)) return s
    do (setq s (sb-mpfr:add s
                            (sb-mpfr:div
                              (sb-mpfr:abs
                                (sb-mpfr:sub z (sb-mpfr:round z))) n)))))



; End of the functions
; ====================

(defun nested (a b)
  (loop
    with x = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for i in (reverse a)
    for j in (reverse b)
    do (setq x (sb-mpfr:sqrt
                 (sb-mpfr:add (sb-mpfr:coerce i 'sb-mpfr:mpfr-float)
                              (sb-mpfr:mul (sb-mpfr:coerce j
                                                           'sb-mpfr:mpfr-float) x))))
    finally (return x)))

(defun nested2 (a b) ; cubic root
  (loop
    with x = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for i in (reverse a)
    for j in (reverse b)
    do (setq x (sb-mpfr:cubic-root
                 (sb-mpfr:add (sb-mpfr:coerce i 'sb-mpfr:mpfr-float)
                              (sb-mpfr:mul (sb-mpfr:coerce j
                                                           'sb-mpfr:mpfr-float) x))))
    finally (return x)))

(defun nested3 (a b) ; cubic root
  (loop
    with x = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for i in (reverse a)
    for j in (reverse b)
    do (setq x (sb-mpfr:k-root
                 (sb-mpfr:add (sb-mpfr:coerce i 'sb-mpfr:mpfr-float)
                              (sb-mpfr:mul (sb-mpfr:coerce j
                                                           'sb-mpfr:mpfr-float) x)) 4))
    finally (return x)))

(defun nested4 (a b) ; cubic root
  (loop
    with x = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for i in (reverse a)
    for j in (reverse b)
    do (setq x (sb-mpfr:k-root
                 (sb-mpfr:add (sb-mpfr:coerce i 'sb-mpfr:mpfr-float)
                              (sb-mpfr:mul (sb-mpfr:coerce j
                                                           'sb-mpfr:mpfr-float) x)) 5))
    finally (return x)))

(defun nested* (f a b)
  (loop
    with x = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    for i in (reverse a)
    for j in (reverse b)
    do (setq x (funcall f
                 (sb-mpfr:add (sb-mpfr:coerce i 'sb-mpfr:mpfr-float)
                              (sb-mpfr:mul (sb-mpfr:coerce j
                                                           'sb-mpfr:mpfr-float) x))))
    finally (return x)))
    
(defun contfrac (a b)
  (loop
    with p1 = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
    with p2 = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    with q1 = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
    with q2 = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
    for ac in a
    for bc in b
    do (let ((p (sb-mpfr:add
                  (sb-mpfr:mul (sb-mpfr:coerce ac 'sb-mpfr:mpfr-float) p2)
                  (sb-mpfr:mul (sb-mpfr:coerce bc 'sb-mpfr:mpfr-float) p1)))
             (q (sb-mpfr:add
                  (sb-mpfr:mul (sb-mpfr:coerce ac 'sb-mpfr:mpfr-float) q2)
                  (sb-mpfr:mul (sb-mpfr:coerce bc 'sb-mpfr:mpfr-float) q1))))
         (progn
           (setq p1 p2)
           (setq p2 p)
           (setq q1 q2)
           (setq q2 q)))
    finally (return (sb-mpfr:div p2 q2))))

(defun hypergeometric-eval (f n)
  (loop
    with g = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
    with s = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
    for i from 1 to 256
    do (progn
         (loop
           for k1 in (caar f)
           for k2 in (caadr f)
           do (setf g (sb-mpfr:mul g (sb-mpfr:coerce (+ k1 (* k2 n) (1- i)) 'sb-mpfr:mpfr-float))))
         (loop
           for k1 in (cadar f)
           for k2 in (cadadr f)
           do (setf g (sb-mpfr:div g (sb-mpfr:coerce (+ k1 (* k2 n) (1- i)) 'sb-mpfr:mpfr-float)))) 
         (setf g (sb-mpfr:mul g (sb-mpfr:coerce (/ (caddr f) i) 'sb-mpfr:mpfr-float)))
         (setf s (sb-mpfr:add s g)))
    finally (return s)))

; evaluate hypergeometric function for n = 1..64
(defun hypergeometric-eval-vector (f)
  (loop
    for n from 1 to 128
    collect (hypergeometric-eval f n)))

(defun is-rational (x)
  (loop
    with one = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
    with mymax = (sb-mpfr:coerce 1e6 'sb-mpfr:mpfr-float)
    for i from 1 to 8
    for j = x then (sb-mpfr:div one (sb-mpfr:sub x (sb-mpfr:floor x)))
    when (sb-mpfr:> j mymax) return T
    finally (return NIL)))

(defun process (desc x)
  (if (sb-mpfr:regularp  x)
    (progn
      (format T "BEGIN RESULT")
      (loop
        for i in desc
        do (print i))
      (format T "~%~S~%" x)
    ; (format T "is-rational ~A~%" (is-rational x))
      (finish-output)
      (sb-ext:run-program "/home/baruchel/.local/bin/myidentify"
                          (list (format NIL "~S" x))
                          :output T)
      (finish-output)
      (format T "END RESULT~%")
      (finish-output))))


; Main loops
; ==========

; (loop
;   with a = (cross-self*
;              (circular-generator
;                (list
;                  (circular-generator
;                    (list
;                      (interesting-hypergeometric-generator 2 1 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))
;                      (interesting-hypergeometric-generator 3 2 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))
;                      (interesting-hypergeometric-generator 4 3 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))))
;                  (mix-generator 
;                    (patterns-generator :negative T)
;                    128
;                    (list #'binomial-transform-sequences
;                          #'euler-transform-sequences
;                          #'stirling-transform-sequences)
;                    :filter (lambda (s) (not (member 0 s))))))
;              ; filter : at least one hypergeometric
;              (lambda (s) (listp (car s))))
;   for seq = (funcall a) then (funcall a)
;   do (let* ((a (car seq)) (b (cadr seq))
;               (a2 (if (listp (car a)) (hypergeometric-eval-vector a) a))
;               (b2 (if (listp (car b)) (hypergeometric-eval-vector b) b)))
;          (progn
;            (process (cons '*contfrac* seq) (contfrac a2 b2)))))

(loop
  with a = (cross
	     ; generator for the filter function
	     (cross
	       ; numerators of sequences
	       (mix-generator 
	         (binomial-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
	         128
	         (list #'binomial-transform-sequences
	               #'identity-transform-sequences
	               #'stirling-transform-sequences)
                 :filter (lambda (s) (not (member 0 s))))
	       ; denominators of sequences
	       (mix-generator 
	         (euler-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
	         128
	         (list #'binomial-transform-sequences
	               #'identity-transform-sequences
	               #'exp-transform-sequences
	               #'stirling-transform-sequences)
                   :filter (lambda (s) (and (not (member 0 s))
		  			  (> (abs (car (last s))) 1000)
					  )))
	     ) ; end of the rationals
             ; second generator (takagi argument)
	     (mix-generator 
	       (patterns-generator :negative T)
	       128
	       (list #'binomial-transform-sequences
	             #'euler-transform-sequences
	             #'stirling-transform-sequences)
               :filter (lambda (s)
                         (loop
                           for k in s
                           for n = 1 then (* n 2)
                           always (and (>= k 0) (<= k n)))))
	     )
  for seq = (funcall a) then (funcall a)
  do (let ((rat (loop
                  for x in (caar seq)
                  for y in (cadar seq)
                  collect (/ x y))))
       (progn
         (process (cons '*takagi-sum-rationals-coefficients* seq)
                  (loop for r in rat
                        for k in (cadr seq)
                        for n = 1 then (* 2 n)
                        with s = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
                        do (setq s (sb-mpfr:add s (sb-mpfr:mul (sb-mpfr:coerce r 'sb-mpfr:mpfr-float) (takagi (/ k n)))))
                        finally (return s)))
         )))

(loop
  with a = (filter
	     ; generator for the filter function
	     (cross
	       ; numerators of sequences
	       (mix-generator 
	         (binomial-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
	         128
	         (list #'binomial-transform-sequences
	               #'identity-transform-sequences
	               #'stirling-transform-sequences)
                 :filter (lambda (s) (not (member 0 s))))
	       ; denominators of sequences
	       (mix-generator 
	         (euler-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
	         128
	         (list #'binomial-transform-sequences
	               #'identity-transform-sequences
	               #'exp-transform-sequences
	               #'stirling-transform-sequences)
                   :filter (lambda (s) (and (not (member 0 s))
		  			  (> (abs (car (last s))) 1000)
					  )))
	     ; end of the generator for the filter function
	     )
	     ; beginning of the filter for the filter function
	     (lambda (s)
               (loop
                 for x in (car s)
                 for y in (cadr s)
                 always (let ((r (/ x y))) (and (>= r 0) (<= r 1)))))
	     ; end of the filter for the filter function
	     )
  for seq = (funcall a) then (funcall a)
  do (let ((rat (loop
                  for x in (car seq)
                  for y in (cadr seq)
                  collect (/ x y))))
       (progn
         (process (cons '*takagi-sum-rationals** seq)
                  (loop for r in rat
                        with s = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
                        do (setq s (sb-mpfr:add s (takagi r)))
                        finally (return s)))
         )))

(loop
  with a = (cross
             (circular-generator
               (list
                 (interesting-hypergeometric-generator2 2 1 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))
                 (interesting-hypergeometric-generator2 3 2 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))
                 ; (interesting-hypergeometric-generator 4 3 '(1 -1 1/2 -1/2 1/3 -1/3 1/4 -1/4 2 -2))
                 ))
             (mix-generator 
               (patterns-generator :negative T)
               128
               (list #'binomial-transform-sequences
                     #'euler-transform-sequences
                     #'exp-transform-sequences
                     #'stirling-transform-sequences)
               :filter (lambda (s) (not (member 0 s)))))
  for seq = (funcall a) then (funcall a)
  do (progn
      (process (cons '*series-takagi-hypergeometric* seq)
               (loop
                 with s = (sb-mpfr:coerce 0 'sb-mpfr:mpfr-float)
                 for x in (hypergeometric-eval-vector (car seq))
                 for y in (cadr seq)
                 do (setq s (sb-mpfr:add s (sb-mpfr:div (takagi x)
                                                        (sb-mpfr:coerce y 'sb-mpfr:mpfr-float))))
                 finally (return s)))
      ))


; (loop
;   with a = (cross-self*
; 	     ; generator for the cross-self* function
; 	     (mix-generator 
; 	       (euler-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
; 	       128
; 	       (list #'binomial-transform-sequences
; 		     #'stirling-transform-sequences)
;                :filter (lambda (s) (not (member 0 s))))
; 	     ; filter for the 'cross-self* function
;              (lambda (s) (> (apply #'+ (diff (mapcar #'abs s))) 51))
; 	     ; end of the filter for the cross-self* function
; 	     )
;   for seq = (funcall a) then (funcall a)
;   do (progn
;        (process (cons '*nested-sqrt* seq) (nested (car seq) (cadr seq)))
;        (process (cons '*nested-cubic* seq) (nested2 (car seq) (cadr seq)))
;      ; (process (cons '*nested-4-root* seq) (nested3 (car seq) (cadr seq)))
;      ; (process (cons '*nested-5-root* seq) (nested4 (car seq) (cadr seq)))
;        (process (cons '*contfrac* seq) (contfrac (car seq) (cadr seq)))
;        ))
; (loop
;   with a = (cross-self*
; 	     ; generator for the cross-self function
; 	     (cross
; 	       ; numerators of sequences
; 	       (mix-generator 
; 	         (binomial-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
; 	         128
; 	         (list #'binomial-transform-sequences
; 	               #'identity-transform-sequences
; 	               #'stirling-transform-sequences)
;                  :filter (lambda (s) (not (member 0 s))))
; 	       ; denominators of sequences
; 	       (mix-generator 
; 	         (euler-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
; 	         128
; 	         (list #'binomial-transform-sequences
; 	               #'identity-transform-sequences
; 	               #'stirling-transform-sequences)
;                    :filter (lambda (s) (and (not (member 0 s))
; 		  			  (> (abs (car (last s))) 1000)
; 					  )))
; 	     ; end of the generator for the cross-self* function
; 	     )
; 	     ; beginning of the filter for the cross-self* function
; 	     (lambda (s)
; 	       (> (denominator
; 		    (/ (car (last (car s))) (car (last (cadr s)))))
; 		    1000))
; 	     ; end of the filter for the cross-self* function
; 	     )
;   for seq = (funcall a) then (funcall a)
;   do (let ((numA (car (car seq)))
; 	   (denA (cadr (car seq)))
; 	   (numB (car (cadr seq)))
; 	   (denB (cadr (cadr seq))))
;        (let ((seqA (loop
; 		     for i in numA
; 		     for j in denA
; 		     collect (/ i j)))
; 	     (seqB (loop
; 		     for i in numB
; 		     for j in denB
; 		     collect (/ i j))))
;          (progn
;               (process (cons '*nested-sqrt* seq) (nested seqA seqB))
;               (process (cons '*nested-cubic* seq) (nested2 seqA seqB))
;               (process (cons '*contfrac* seq) (contfrac seqA seqB))
;               ))))

(defun seq-as-ser (s fact n)
  (loop
    for i from 0 to (1- n)
    collect
      (sb-mpfr:coerce 
        (loop
          for j from 0
          for c in s
          for x = 1 then (* x i)
          sum (/ (* x c) (aref fact j)))
          'sb-mpfr:mpfr-float)))

(defun not-finite (s)
  (let ((r (reverse s)))
    (or (not (zerop (car r))) (not (zerop (cadr r))))))

; (loop ; test with series
;   with a = (cross-self
; 	     ; generator for the cross-self* function
; 	     (mix-generator 
; 	       (patterns-generator :negative T)
; 	       128
; 	       (list #'binomial-transform-sequences
; 		     #'euler-transform-sequences
; 		     #'stirling-transform-sequences)
;                :filter #'not-finite
;                )
; 	     )
;   with factorial =
;         (multiple-value-bind (bin fact)
;            (funcall *binomial-maker* 127) fact)
;   for seq = (funcall a) then (funcall a)
;   do (progn
;        (process (cons '*contfrac-with-series* seq)
;                 (contfrac (seq-as-ser (car seq) factorial 64)
;                           (seq-as-ser (cadr seq) factorial 64)))
;        ))


; (loop
;   with a = (cross
;              ; simpe patterns either containing zeros or equal to '(1)
;              (filter
;                (patterns-generator :negative T)
;                (lambda (s) (or (equal '(1) s) (member 0 s))))
;              ; various transforms containing no zero
; 	     (mix-generator 
; 	       (patterns-generator :negative T)
; 	       128
; 	       (list #'binomial-transform-sequences
; 	             #'euler-transform-sequences
; 	             #'stirling-transform-sequences)
;                :filter (lambda (s) (not (member 0 s)))))
;   for seq = (funcall a) then (funcall a)
;   do (progn
;        (process (cons '*contfrac-as-functions-zeros* seq)
;                 (contfrac (two-seq-as-contfrac-function (caar seq) (cadar seq) 64)
;                           (two-seq-as-contfrac-function (caadr seq) (cadadr seq) 64)))
;        ))


(defun infinite-zeros (s)
  (loop
    for i from 1 to 5
    for v in (reverse s)
    when (zerop v) return T
    finally (return NIL)))

(defun not-finite-zeros (s)
  (or (not (member 0 s)) (infinite-zeros s)))

(defun at-least-one-with-infinite-zeros (s) ; s is a couple of sequences
  (or (infinite-zeros (car s)) (infinite-zeros (cadr s))))


; see sequences a and b as a function where each 0 actually is 'n
; then compute the continue dfraction for n=1, n=2, n=3, etc.
; and return the 'nbr' according values
(defun two-seq-as-contfrac-function (a b nbr)
  (loop
    for i from 1 to nbr
    collect
      (contfrac (substitute i 0 a) (substitute i 0 b))))

; (loop
;   with a = (cross-self*
;              (filter
; 	       (cross-self
; 	         (euler-transform-sequences 128 (patterns-generator :negative T)
;                                             :filter #'not-finite-zeros))
;                ; filter
;                 (lambda (c)
;                   (loop
;                     for k in (two-seq-as-contfrac-function (car c) (cadr c) 24)
;                     when (sb-mpfr:< (sb-mpfr:abs k) (sb-mpfr:coerce 1e-24 'sb-mpfr:mpfr-float)) return NIL
;                     when (sb-mpfr:> (sb-mpfr:abs k) (sb-mpfr:coerce 1e24 'sb-mpfr:mpfr-float)) return NIL
;                     finally (return T))))
;                #'at-least-one-with-infinite-zeros
; 	     )
;   for seq = (funcall a) then (funcall a)
;   do (progn
;        (process (cons '*contfrac-as-functions-zeros* seq)
;                 (contfrac (two-seq-as-contfrac-function (caar seq) (cadar seq) 64)
;                           (two-seq-as-contfrac-function (caadr seq) (cadadr seq) 64)))
;        ))

(defun seq-as-ogf-reciprocal (s n) ; sz=initial size, n=nbr.iter.
  (let ((c (loop
             for i in (car s)
             for j in (cadr s)
             collect (sb-mpfr:coerce (/ i j) 'sb-mpfr:mpfr-float))))
    (loop
      for i from 1 to n
      collect
        (loop
          for k in c
          for x = (sb-mpfr:coerce 1 'sb-mpfr:mpfr-float)
                  then (sb-mpfr:div x (sb-mpfr:coerce i 'sb-mpfr:mpfr-float))
          for s = (car c) then (sb-mpfr:add s (sb-mpfr:mul k x))
          finally (return s)))))

; (loop
;   with a = (cross-self
; 	     ; generator for the cross-self function
; 	     (cross*
; 	       ; numerators of sequences
; 	       (mix-generator 
; 	         (patterns-generator :negative T)
; 	         128
; 	         (list #'binomial-transform-sequences
; 	               #'euler-transform-sequences
; 	               #'stirling-transform-sequences)
;                  :filter #'not-finite
;                )
; 	       ; denominators of sequences
; 	       (stirling-transform-sequences 128 (patterns-generator)
;                  :filter (lambda (s) (and (not (member 0 s))
; 					  (> (abs (car (last s))) 1d64)
; 					  )))
;                ; filer for cross*
;                (lambda (s) (> (denominator (/ (car (last (car s)))
;                                               (car (last (cadr s))))) 1d64))
; 	     ; end of the generator for the cross-self function
; 	     ))
;   for seq = (funcall a) then (funcall a)
;   do (progn
;        (process (cons '*contfrac-with-ogf-reciprocal* seq)
;                 (contfrac (seq-as-ogf-reciprocal (car seq) 64)
;                           (seq-as-ogf-reciprocal (cadr seq) 64)))
;        ))
; 
; (loop
;   with a = (cross-self
; 	     ; generator for the cross-self* function
; 	     (mix-generator 
; 	       (patterns-generator :negative T)
; 	       128
; 	       (list #'binomial-transform-sequences
; 		     #'euler-transform-sequences
; 		     #'stirling-transform-sequences)
;                :filter (lambda (s) (not (member 0 s))))
; 	     )
;   for seq = (funcall a) then (funcall a)
;   do (progn
;   ;    (process (cons '*nested-sqrt* seq) (nested (car seq) (cadr seq)))
;   ;    (process (cons '*nested-cubic* seq) (nested2 (car seq) (cadr seq)))
;   ;    (process (cons '*nested-4-root* seq) (nested3 (car seq) (cadr seq)))
;   ;    (process (cons '*nested-5-root* seq) (nested4 (car seq) (cadr seq)))
;        (process (cons '*nested-atan* seq) (nested* #'sb-mpfr:log (car seq) (cadr seq)))
;   ;    (process (cons '*nested-contfrac* seq) (contfrac (car seq) (cadr seq)))
;        ))
; 
; (defun ad-hoc2 (s)
;   (and (> (car s) 0)
;        (increasing-non-polynomial s))) ; non strict
; 
; (loop
;   with a = (cross-self
; 	     ; generator for the cross-self* function
; 	     (mix-generator 
; 	       (patterns-generator :negative T)
; 	       128
; 	       (list #'binomial-transform-sequences
; 		     #'euler-transform-sequences
; 		     #'stirling-transform-sequences)
;                :filter #'ad-hoc2)
; 	     )
;   for seq = (funcall a) then (funcall a)
;   do (progn
;   ;    (process (cons '*nested-sqrt* seq) (nested (car seq) (cadr seq)))
;   ;    (process (cons '*nested-cubic* seq) (nested2 (car seq) (cadr seq)))
;   ;    (process (cons '*nested-4-root* seq) (nested3 (car seq) (cadr seq)))
;   ;    (process (cons '*nested-5-root* seq) (nested4 (car seq) (cadr seq)))
;        (process (cons '*contfrac* seq) (contfrac (car seq) (cadr seq)))
;        ))
; 
; 
(loop
  with a = (cross-self*
	     ; generator for the cross-self function
	     (cross
	       ; numerators of sequences
	       (mix-generator 
	         (euler-transform-sequences 128 (patterns-generator :negative T) :filter NIL)
	         128
	         (list #'binomial-transform-sequences
	               #'identity-transform-sequences
	               #'stirling-transform-sequences)
                 :filter (lambda (s) (not (member 0 s))))
	       ; denominators of sequences
	       (exp-transform-sequences 128 (patterns-generator)
                 :filter (lambda (s) (and (not (member 0 s))
					  (> (abs (car (last s))) 1000)
					  )))
	     ; end of the generator for the cross-self* function
	     )
	     ; beginning of the filter for the cross-self* function
	     (lambda (s)
	       (> (denominator
		    (/ (car (last (car s))) (car (last (cadr s)))))
		    1000))
	     ; end of the filter for the cross-self* function
	     )
  for seq = (funcall a) then (funcall a)
  do (let ((numA (car (car seq)))
	   (denA (cadr (car seq)))
	   (numB (car (cadr seq)))
	   (denB (cadr (cadr seq))))
       (let ((seqA (loop
		     for i in numA
		     for j in denA
		     collect (/ i j)))
	     (seqB (loop
		     for i in numB
		     for j in denB
		     collect (/ i j))))
         (progn
            ; (process (cons '*nested-sqrt* seq) (nested seqA seqB))
            ; (process (cons '*nested-cubic* seq) (nested2 seqA seqB))
              (process (cons '*contfrac* seq) (contfrac seqA seqB))
              ))))


; (loop
;   with a = (cross2b
;              (euler-transform-sequences 8
;                   (patterns-generator :negative T)
;                   :filter (lambda (s) (not (member 0 s))))
;              (lambda (s) (member 2 s)))
;   for seq = (funcall a) then (funcall a)
;   for i from 1 to 20
;   do (print seq))

; (loop
;   with a = (euler-transform-sequences 24
;                   (patterns-generator :negative T)
;                   :filter #'increasing-non-polynomial)
;   for seq = (funcall a) then (funcall a)
;   for i from 1 to 40
;   do (print seq))


; (test #'euler-transform-sequences '(2 1))
(defun test (f pat)
  (funcall (funcall f 24 (lambda () pat))))
