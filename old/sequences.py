# -*- coding: utf-8 -*-

# (Python 3 only)

# edit the end of the file according to your own needs


def precompile_sequence_iterator(size):

    # Following functions (make_plan and make_reciprocal_plan come form my other
    # project 'karatsuba')

    def compile_plan(name, d):
        context = {}
        exec(d%name, {}, context)
        return context[name]

    def make_plan(l1, l2, plan=None, raw=False, stats = None):
        # Internal functions
        class _atom:
            def __init__(self, op):
                self.is_zero = False
                self.children = []
                self.parents = []
                self.ref = None
                self.content = None
                self.op = op
                self.top = None
            def __hash__(self): return id(self)
            def __eq__(self, arg): return id(self) == id(arg)
        def _add(l1, l2, level):
            r = []
            for a, b in zip(l1, l2):
                if b.is_zero: # both a=0 and b=0 or only b=0
                    r.append(a)
                elif a.is_zero:
                    r.append(b)
                else:
                    c = _atom("add")
                    c.children = [a, b]
                    a.parents.append(c)
                    b.parents.append(c)
                    r.append(c)
            for a in r:
                a.level = level
            return r
        def _sub(l1, l2, level):
            r = []
            for a, b in zip(l1, l2):
                if b.is_zero: # both a=0 and b=0 or only b=0
                    r.append(a)
                elif a.is_zero:
                    c = _atom("neg")
                    c.children = [b]
                    b.parents.append(c)
                    r.append(c)
                else:
                    c = _atom("sub")
                    c.children = [a, b]
                    a.parents.append(c)
                    b.parents.append(c)
                    r.append(c)
            for a in r:
                a.level = level
            return r
        def _karatsuba(l1, l2, level=0):
            if len(l1) == 1:
                if l1[0].is_zero or l2[0].is_zero:
                    x = _atom("0")
                    x.is_zero = True
                    x.content = "0"
                    x.level = level
                else:
                    x = _atom("mul")
                    x.children = [l1[0], l2[0]]
                    x.level = level
                    l1[0].parents.append(x)
                    l2[0].parents.append(x)
                y = _atom("0")
                y.is_zero = True
                y.content = "0"
                return [x, y]
            n = len(l1)
            m = n // 2
            b, a = l1[:m], l1[m:]
            d, c = l2[:m], l2[m:]
            X = _karatsuba(a, c, level + 1)
            Y = _karatsuba(b, d, level + 1)
            Z = _karatsuba(_sub(a, b, level), _sub(c, d, level), level + 1)
            T = _sub(_add(X, Y, str(level)+"b"), Z, str(level)+"b") # TODO: level + 0.5 ?
            return Y[:m] + _add( Y[m:] + X[:m], T, level) + X[m:]
        def _parse_tree(t, parse):
            if (t.op != "origin" or t.top != None) and not t.is_zero:
                parse.append(t)
            for c in t.children: _parse_tree(c, parse)
        # Main function
        try:
            l1 = [None if x==None else int(x) for x in l1]
            l2 = [None if x==None else int(x) for x in l2]
        except:
            raise TypeError
        n1, n2 = len(l1), len(l2)
        if n1 != n2:
            raise ValueError("Input lists have different lengths")
        if plan == None:
            plan = [True]*(2*n1)
        elif len(plan) != (2*n1):
            raise ValueError("Optional 'plan' parameter should be twice"
                            + " as long as input lists")
        if n1 ^ (1<<(n1).bit_length()>>1) != 0:
            raise ValueError("Length of input lists should be a power of 2")
        x, y = [], []
        for k in range(len(l1)):
            if l1[k] != None:
                a = _atom("origin")
                a.is_zero = False
                a.content = "u["+str(l1[k])+"]"
                a.ref = a.content
                x.append(a)
            else:
                a = _atom("0")
                a.is_zero = True
                a.content = "0"
                x.append(a)
            if l2[k] != None:
                a = _atom("origin")
                a.is_zero = False
                a.content = "v["+str(l2[k])+"]"
                a.ref = a.content
                y.append(a)
            else:
                a = _atom("0")
                a.is_zero = True
                a.content = "0"
                y.append(a)
        # Parse tree
        n, parse = 0, []
        for i, k in enumerate(_karatsuba(x, y)):
            if plan[i]:
                k.top = n
                _parse_tree(k, parse)
                n += 1
        parse = list(set(parse))
        # Compile tree
        n, out = 0, []
        stats = {} if stats == None else stats
        stats['mul'], stats['add'], stats['sub'], stats['neg'] = 0, 0, 0, 0
        while len(parse) > 0:
            parse.sort(key=lambda x: len([c for c in x.children if c.ref==None]))
            i = 0
            while len([c for c in parse[i].children if c.ref==None]) == 0:
                if parse[i].op == "mul":
                    parse[i].content = "*".join(
                            [c.ref for c in parse[i].children])
                    stats['mul'] += 1
                elif parse[i].op == "add":
                    parse[i].content = "+".join(
                            [c.ref for c in parse[i].children])
                    stats['add'] += 1
                elif parse[i].op == "sub":
                    parse[i].content = (parse[i].children[0].ref + "-"
                                        + parse[i].children[1].ref)
                    stats['sub'] += 1
                elif parse[i].op == "neg":
                    parse[i].content = ("-" + parse[i].children[0].ref)
                    stats['neg'] += 1
                elif parse[i].op == "origin":
                    parse[i].content = parse[i].content
                if parse[i].top == None:
                    l = "t.append(" + parse[i].content + ")"
                    parse[i].ref = "t["+str(n)+"]"
                    l += " #  -->  " + parse[i].ref
                    n += 1
                else:
                    l = "r["+str(parse[i].top)+"]="+parse[i].content
                    parse[i].ref = "r["+str(parse[i].top)+"]"
                try:
                    l += "  # reclevel=" + str(parse[i].level)
                except:
                    pass
                out.append(l)
                i += 1
                if i == len(parse): break
            parse = parse[i:]
        d =  "def %s(u, v):\n"
        d += "    t, r = [], [0]*"+str(sum(plan))+"\n"
        d += "    " + "\n    ".join(out) + "\n"
        d += "    return r    # Stats: "+str(stats)
        if raw: return (d%"convolution")
        return compile_plan("convolution", d)

    def make_reciprocal_plan(s):
        try:
            s = [None if x==None else int(x) for x in s]
        except:
            raise TypeError
        if s[0] == None:
            raise ValueError("Initial coefficient should not be None")
        n, z = len(s), s[0]
        if n == 1:
            return lambda l: [ 1/l[z] ]
        if n ^ (1<<(n).bit_length()>>1) != 0:
            raise ValueError("Length of input list should be a power of 2")
        n = n.bit_length() - 1
        s1 = [ make_plan( range(2**i, 2**(i+1)), range(2**i, 2**(i+1)))
                 for i in range(n-1) ]
        s2 = [ make_plan( range(2**i), range(2**i, 2**(i+1)))
                 for i in range(n-1) ]
        p = [ make_plan( range(2**i), s[:2**i],
                         plan = [False]*(2**(i-1))
                                + [True]*(2**(i-1))
                                + [False]*(2**i) )
                for i in range(2, n+1) ]
        n -= 1
        w = s[1]
        if w == None:
            def reciprocal(l):
                # WARNING: the following line was initially:
                #     r = [1/l[z]]
                # but it was hacked the following way because it is known the
                # initial coefficient will always be 1 in this project
                r = [1//l[z]]
                l = [ -x for x in l ]
                S = [ r[0]*r[0], 0 ]
                for i in range(n):
                    S += s1[i](r,r)
                    for j, k in enumerate(s2[i](r,r)):
                        S[(1<<i) + j] += 2*k
                    r += p[i](S, l)
                return r
        else:
            def reciprocal(l):
                # WARNING: the following line was initially:
                #     r = [1/l[z]]
                # but it was hacked the following way because it is known the
                # initial coefficient will always be 1 in this project
                r = [1//l[z]]
                l = [ -x for x in l ]
                S = [ r[0]*r[0], 0 ]
                r.append( S[0]*l[w] )
                for i in range(n):
                    S += s1[i](r,r)
                    for j, k in enumerate(s2[i](r,r)):
                        S[(1<<i) + j] += 2*k
                    r += p[i](S, l)
                return r
        return reciprocal

    ################### END OF MODULE karatsuba ##########################


    # function for convolution
    convolution = make_plan(range(size), range(size),
                            plan=[True]*size+[False]*size)

    # function for inverting a power series (as a list)
    reciprocal = make_reciprocal_plan(range(size))



    # pascal_row(n)
    #   --> returns the nth row of Pascal triangle
    def pascal_row():
        rows = [ [ 1 ] ]
        def f(n):
            while len(rows) <= n:
                r = rows[-1]
                l = [ 1 ]
                for k in range(len(r)-1):
                    l.append( r[k] + r[k+1] )
                l.append(1)
                rows.append(l)
            return rows[n]
        return f
    pascal_row = pascal_row()



    # compute  1/(1 - x^i)^n
    def euler_term():
        table = []
        def f(i, n):
            if i < len(table):
                t = table[i]
                if n < len(t) and t[n]: return t[n]
                else:
                    while len(t) <= n: t.append(None)
            else:
                while len(table) < i:
                    table.append([])
                table.append([None]*(n+1))
            l, s, o = [ 0 ] * size, 1, 0
            for k in pascal_row(n):
                l[o], s, o = s*k, -s, o + i
                if o >= size: break
            l = reciprocal(l)
            table[i][n] = l
            return l
        return f
    euler_term = euler_term()



    def euler_transform(a):
        l = euler_term(1, a[0])
        for k in range(1, len(a)):
            l = convolution(l, euler_term(k+1, a[k]))
        return l


    def iterator_rec(pattern, s, cursum, p, start=0):
        for k in range(start, s-cursum+1):
            pattern[p] = k
            if p == 1:
                pattern[0] = s-cursum-k
                yield pattern
            else:
                yield from iterator_rec(pattern, s, cursum+k, p-1)

    def iterator():
        s = 1
        pattern = [ 1 ]
        yield pattern
        while True:
            pattern.append(0)
            s += 1
            # all patterns of size 's' with sum < s
            # (and last term being at least 1)
            for n in range(1, s):
                yield from iterator_rec(pattern, n, 0, s-1, 1)
            # all patterns of size 's' with sum == s
            yield from iterator_rec(pattern, s, 0, s-1)

    def sequences(filter=None):
        for pattern in iterator():
            e = euler_transform(pattern)
            if filter:
                if filter(e): yield e
            else: yield e

    return sequences

####### End of code for generator of sequences ########################"

sequences = precompile_sequence_iterator(32)
# Now everything is precompiled into 'sequences' which can be reused
# several times because it isn't itself an iterator as long as it isn't
# called (thus it can be called several times with different filters
# for instance). Different iterators coming from the same call to the
# main precompile_sequence_iterator function will share their data
# in regards to memoization.

for s in sequences():
    print(s)
