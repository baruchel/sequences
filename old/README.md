# Sequences

_An iterator (for Python 3 only) building thousands of "interesting" (from an arithmetical point of view) sequences of 16, 32, 64 terms (or any power of 2)._

This script is intended to be used for _experimental research_. The iterator can
be used for feeding any algorithm in order to help discovering new identities.
Many of these sequences can also be found in the [OEIS](http://oeis.org/).

The script is intended to be used with several implementations of Python3; no
external module is required. It works with the standard CPython implementation
but also with the quicker Pypy implementation.

The algorithm is intended to be as fast as possible by heavily caching previously computed results; convolutions are performed with an opimized [Karatsuba implementation](https://github.com/baruchel/karatsuba).

## Usage

Just edit the file and make any changes according to your own needs (**at the
end of the file**):

    for seq in sequences():
        print(seq)

An optional filter can be specified for discarding unwanted sequences:

    for seq in sequences(filter=lambda s: 0 not in s):
        print(seq)

The iterator is intended to be very easy to use; if an interesting sequence is
discovered, the user will be able to guess an easy generating function for it
later. However, if the algorithm really needs to know the formula for the last
generated sequence, it can still try something like:

    S = sequences()
    for s in S:
        if is_interesting(s):
            pattern = S.gi_frame.f_locals['pattern']
            print(pattern, s)

where the `pattern` in the code above means that the sequence is the [Euler
transform](https://oeis.org/wiki/Euler_transform) of this pattern. Since the same `pattern` internal variable is used during the whole computation, the reference will not change:

    S = sequences()
    _ = next(S)     # throw away the first sequence [1, 1, 1, 1, ...]
    pattern = S.gi_frame.f_locals['pattern']

    for s in S:
        if is_interesting(s):
            print(pattern, s) # print the pattern of the last generated sequence

The script can be tuned for changing the length of the sequences, but a power of
2 should be specified.
